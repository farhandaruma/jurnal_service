import psycopg2 as pg
from db_config import config
from datetime import datetime, timedelta
import os
import ast
import pprint
import csv
import json

params = config()

def get_pending_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM daruma_jurnal_jurnal_jobs WHERE status='pending'
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            data_input = row[1]
            status = row[2]
            response = row[3]
            created_at = row[4]
            result.append(dict(
                job_id=job_id,
                data_input=data_input,
                status=status,
                response=response,
                created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_all_jobs():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM daruma_jurnal_jurnal_jobs
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            job_id = row[0]
            data_input = row[1]
            status = row[2]
            response = row[3]
            created_at = row[4]
            result.append(dict(
                job_id=job_id,
                data_input=data_input,
                status=status,
                response=response,
                created_at=created_at
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def delete_all_rows():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    DELETE FROM daruma_jurnal_jurnal_job
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def update_job(job_id, response):
    """ insert vendor into vendors table """

    # sql = """
    # INSERT INTO request(job_id, url, http_status_code, content_type) VALUES(%s, %s, %s, %s)
    # """

    sql = """
    UPDATE daruma_jurnal_jurnal_jobs SET status='done', response=%s WHERE job_id=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (response, job_id,))

        # commit the changes
        conn.commit()
        print("job %s was updated.." % (job_id))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()
