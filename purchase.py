from main import *
import pprint
import traceback
import csv
import datetime
import time
import json
# po = get_purchase_order('PO-000214').json()
# pd = get_purchase_delivery('PD-000226').json()
# pi = get_purchase_invoice('1206283,').json()
# # pprint.pprint(po)
# # pprint.pprint(pi)
# transaction_lines_attributes = pd['purchase_delivery']['transaction_lines_attributes']
# list_of_products = [i['product']['product_custom_id'] for i in transaction_lines_attributes]
# print(list_of_products)
# # pprint.pprint(pi)


def purchase(data, warehouse_name):
    data = data['data']
    daruma_address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."

    due_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                             data['invoices'][0]['invoice_date'][5:7],
                             data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else "%s/%s/%s" % (
        datetime.datetime.now().day, datetime.datetime.now().month, datetime.datetime.now().year)
    shipping_date = "%s/%s/%s" % (data['deliveries'][0]['delivery_date'][8:10],
                                  data['deliveries'][0]['delivery_date'][5:7],
                                  data['deliveries'][0]['delivery_date'][:4]) if len(data['deliveries']) > 0 else None
    shipping_price = data['invoices'][0]['shipping_fee'] if len(data['invoices']) > 0 else None
    shipping_address = data['deliveries'][0]['ship_to'] if len(data['deliveries']) > 0 and data['deliveries'][0][
        'ship_to'] is not None else daruma_address
    paid_date = "%s/%s/%s" % (data['payments'][0]['paid_at'][8:10],
                              data['payments'][0]['paid_at'][5:7],
                              data['payments'][0]['paid_at'][:4]) if len(data['payments']) > 0 else None
    ship_via = data['shipping_method'] if data['shipping_method'] is not 'None' else None
    tracking_no = None
    address = "Komplek Griya Inti Sentosa, Jalan Griya Utara No.3, Sunter Agung, Tanjung Priok, Jakarta Utara, 14350, DKI Jakarta."
    term_name = data['payment_term']
    transaction_date_po = "%s/%s/%s" % (data['order_date'][8:10],
                                        data['order_date'][5:7],
                                        data['order_date'][:4])
    deposit = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else 0
    discount_unit = None
    legal_entity_type =f"{data.get('supplier', None).get('organization', None).get('legal_entity_type', '')}" if data.get('supplier', None) is not None else None
    company_name = f"{legal_entity_type} {data.get('supplier', None).get('organization', None).get('name', None)}" if data.get('supplier', None) is not None else None
    associate_company = company_name if data.get('supplier', None) is not None else None
    person_name = "%s %s".strip() % (data['contact']['first_name'], data['contact']['last_name'])
    email = None
    purchase_delivery_no = data['deliveries'][0]['delivery_no'] if data['deliveries'] else None
    purchase_invoice_no = data['invoices'][0]['invoice_no'] if data['invoices'] else None
    paid_amount = data['payments'][0]['paid_amount'] if len(data['payments']) > 0 else None
    transaction_no = data['order_no']
    memo = None
    products = data['lines']
    payment_method = data['payments'][0]['payment_method'] if len(data['payments']) > 0 else "Cash"
    invoice_date = "%s/%s/%s" % (data['invoices'][0]['invoice_date'][8:10],
                                 data['invoices'][0]['invoice_date'][5:7],
                                 data['invoices'][0]['invoice_date'][:4]) if len(data['invoices']) > 0 else None

    bank = data['payments'][0].get('bank_account', None) if len(data['payments']) > 0 else None

    total_transaction = data['total_incl_tax']

    purchase_order_response = dict(
        order_no=transaction_no,
        purchase_order_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    purchase_delivery_response = dict(
        order_no=transaction_no,
        purchase_delivery_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    purchase_invoice_response = dict(
        order_no=transaction_no,
        purchase_invoice_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    purchase_payment_response = dict(
        order_no=transaction_no,
        purchase_payment_id=None,
        message=None,
        error_message=None,
        jurnal_message=None
    )

    transaction_response = dict(
        # order_no=transaction_no,
        purchase_order=purchase_order_response,
        delivery_order=purchase_delivery_response,
        invoice_order=purchase_invoice_response,
        payment_order=purchase_payment_response,
        warning=[]
    )
    # print("processing for transaction no: %s" % transaction_no)

    if len(data['deliveries']) > 1:
        warning = transaction_response['warning']
        warning.append('this transaction has multiple deliveries data')

    if len(data['invoices']) > 1:
        warning = transaction_response['warning']
        warning.append('this transaction has multiple invoices data')

    if len(data['payments']) > 1:
        warning = transaction_response['warning']
        warning.append('this transaction has multiple payments data')

    checking_po = get_purchase_order(transaction_no=transaction_no).json()

    # cheking purchase order is already exists or not
    if checking_po.get('purchase_order', None):
        # print('[purchase order already exists]')
        purchase_order_response['message'] = 'already exists'
        purchase_order_response['purchase_order_id'] = checking_po['purchase_order']['transaction_no']
    else:
        # create new purchase order
        purchase_order = create_purchase_order(transaction_date=transaction_date_po,
                                               shipping_date=shipping_date,
                                               shipping_price=shipping_price,
                                               shipping_address=shipping_address,
                                               ship_via=ship_via,
                                               tracking_no=tracking_no,
                                               address=address,
                                               term_name=term_name,
                                               due_date=due_date,
                                               discount_unit=discount_unit,
                                               person_name=person_name,
                                               warehouse_name=warehouse_name,
                                               email=email,
                                               transaction_no=transaction_no,
                                               memo=transaction_no,
                                               products=products,
                                               total_transaction=float(total_transaction),
                                               bank=bank)

        # # pprint.pprint(purchase_order)
        if purchase_order.get('error_full_messages', None) is not None:
            # pprint.pprint(purchase_order)
            purchase_order_response['jurnal_message'] = purchase_order.get('error_full_messages')

            if purchase_order.get('error_full_messages', None) == []:
                purchase_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response

            if purchase_order.get('remaining', None):
                print('total transaction: %s' % total_transaction)
                print('deposit: %s' % deposit)
                print('term name: %s' % term_name)
                purchase_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response

        if purchase_order.get("person_name", None):
            # print('creating purchase order failed: creating contact')
            # pprint.pprint(purchase_order)
            new_contact = create_contact(display_name=person_name.strip(),
                                         associate_company=associate_company,
                                         email=None,
                                         address=None,
                                         billing_address=None,
                                         is_customer=False,
                                         is_vendor=True,
                                         people_type='vendor')
            # pprint.pprint(new_contact.json())
            if new_contact.json().get('person', None):
                print(f'{person_name} contact has been created')
            if new_contact.json().get('errors', None) and \
                    new_contact.json().get('errors', None) == 'Custom ID sudah pernah dipakai':
                purchase_order_response['jurnal_message'] = purchase_order.get('error_full_messages')
                purchase_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response

            purchase_order = create_purchase_order(transaction_date=transaction_date_po,
                                                   shipping_date=shipping_date,
                                                   shipping_price=shipping_price,
                                                   shipping_address=shipping_address,
                                                   ship_via=ship_via,
                                                   tracking_no=tracking_no,
                                                   address=address,
                                                   term_name=term_name,
                                                   due_date=due_date,
                                                   discount_unit=discount_unit,
                                                   person_name=person_name,
                                                   warehouse_name=warehouse_name,
                                                   email=email,
                                                   transaction_no=transaction_no,
                                                   memo=transaction_no,
                                                   products=products,
                                                   total_transaction=total_transaction,
                                                   bank=bank)
            # pprint.pprint(purchase_order)
            if purchase_order.get("error_full_messages", None) is not None:
                purchase_order_response['jurnal_message'] = purchase_order.get('error_full_messages')
                purchase_order_response['message'] = 'failed'
                transaction_response['delivery_order'] = 'not created'
                transaction_response['invoice_order'] = 'not created'
                transaction_response['payment_order'] = 'not created'
                return transaction_response
            else:
                purchase_order_response['message'] = 'created'
                purchase_order_response['purchase_order_id'] = purchase_order['purchase_order']['transaction_no']

        if purchase_order.get('error_full_messages', None):
            purchase_order_response['jurnal_message'] = purchase_order.get('error_full_messages')
            purchase_order_response['message'] = 'failed'
            transaction_response['delivery_order'] = 'not created'
            transaction_response['invoice_order'] = 'not created'
            transaction_response['payment_order'] = 'not created'
            return transaction_response
        else:
            purchase_order_response['message'] = 'created'
            # pprint.pprint(purchase_order)
            if purchase_order.get('error_full_messages', None) is not None:
                purchase_order_response['jurnal_message'] = purchase_order.get('error_full_messages', None)
            else:
                purchase_order_response['purchase_order_id'] = purchase_order['purchase_order']['transaction_no']



    # processing purchase deliveries
    deliveries = data.get('deliveries', None)
    if deliveries:

        delivery = deliveries[0]
        checking_delivery = get_purchase_delivery(delivery['delivery_no']).json()

        # if 'purchase_delivery' key is not available in json response, so create new purchase delivery
        if not checking_delivery.get('purchase_delivery', None):
            # create new purchase delivery

            delivery_no = delivery['delivery_no']
            selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)
            max_retries = 0
            while max_retries < 3 and not selected_po_id:
                selected_po_id = get_purchase_order_id_by_transaction_no(transaction_no=transaction_no)
                max_retries += 1
                time.sleep(3)
            if not selected_po_id:
                print(f'transaction no: {transaction_no}')
                print('selected po id is None')
                purchase_delivery_response['message'] = 'failed'
                transaction_response['deliveries'] = 'selected po id is not found'
                transaction_response['invoices'] = 'not created'
                transaction_response['payments'] = 'not created'
                return transaction_response
            purchase_delivery = create_purchase_delivery(person_name=person_name,
                                                         shipping_address=shipping_address,
                                                         transaction_date=shipping_date,
                                                         ship_via=ship_via,
                                                         tracking_no=tracking_no,
                                                         transaction_no=purchase_delivery_no,
                                                         reference_no=None,
                                                         selected_po_id=selected_po_id,
                                                         products=products,
                                                         shipping_price=shipping_price,
                                                         memo=transaction_no,
                                                         message=None,
                                                         custom_id=delivery_no)
            pprint.pprint(purchase_delivery)
            purchase_delivery_response['purchase_delivery_id'] = purchase_delivery['purchase_delivery']['custom_id']
            purchase_delivery_response['message'] = 'created'

        else:
            # print('[deliveries already created]')
            purchase_delivery_response['message'] = 'already exists'
            purchase_delivery_response['purchase_delivery_id'] = checking_delivery['purchase_delivery']['custom_id']
            # convert delivery order to invoice
            # delivery_status = checking_delivery['purchase_delivery']['transaction_status']['name']
            # if str(delivery_status).strip().lower() != 'closed':
            #     custom_id_delivery = checking_delivery['purchase_delivery']['custom_id']
            #     r = convert_to_purchase_invoice(custom_id_delivery, invoice_date)
            #     # pprint.pprint(r.text)

    else:
        # print('deliveries data not available')
        purchase_delivery_response['message'] = 'deliveries data not available'


    # checking invoices data from input data, is it available or not
    invoices = data.get('invoices', None)
    payments = data.get('payments', None)
    deliveries = data.get('deliveries', None)
    delivery = deliveries[0] if deliveries else None
    if invoices and delivery:
        delivery_no = delivery['delivery_no']
        purchase_del = get_purchase_delivery(delivery_no).json().get('purchase_delivery', None)
        get_purchase_invoice_id = purchase_del if purchase_del is not None else None
        get_purchase_invoice_id = get_purchase_invoice_id['purchase_invoice'][ 'id'] if get_purchase_invoice_id is not None and get_purchase_invoice_id.get('purchase_invoice', None) else None
        # try:
        #     purchase_id = get_purchase_invoice(get_purchase_invoice_id).json().get('purchase_invoice', None).get('transaction_no', None)
        # except:
        #     purchase_id = None
        if not purchase_del:
            # pprint.pprint(get_purchase_invoice(get_purchase_invoice_id).json())
            purchase_delivery_response['message'] = 'failed'
            purchase_delivery_response['jurnal_message'] = get_purchase_invoice(get_purchase_invoice_id).json()
            return transaction_response
        if not get_purchase_invoice_id:
            # create purchase invoice and payment if payments data exists
            purchase_delivery_id = get_purchase_delivery(delivery['delivery_no']).json()['purchase_delivery']['id']
            purchase_invoice = convert_to_purchase_invoice(id=purchase_delivery_id, invoice_date=invoice_date)
            purchase_invoice_response['purchase_invoice_id'] = purchase_invoice_no
            purchase_invoice_response['message']='created'
            # pprint.pprint(purchase_invoice.json())
            if str(term_name) != 'CBD':
                print("this is not CBD 1")
                if payments:
                    purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                          transaction_no=transaction_no,
                                                                          transaction_date=paid_date,
                                                                          purchase_id=
                                                                          purchase_invoice.json()['purchase_invoice'][
                                                                              'transaction_no'],
                                                                          amount_receive_payment=paid_amount,
                                                                          payment_method_name=payment_method,
                                                                          memo=transaction_no,
                                                                          bank=bank)

                    if purchase_order_payment.get('error_full_messages', None) is not None:
                        purchase_order_payment['jurnal_message'] = purchase_order_payment.get('error_full_messages')
                        purchase_order_payment['message'] = 'failed'
                        return transaction_response
                    else:
                        purchase_payment_response['message'] = 'created'
                        purchase_payment_response['purchase_payment_id'] = purchase_order_payment['purchase_payment'][
                            'custom_id']

        else:
            # print('[invoices already exists]')
            purchase_invoice_response['message'] = 'already exists'
            purchase_invoice_response['purchase_invoice_id'] = get_purchase_invoice_id
            # if str(term_name) != 'CBD':
            #     if payments and purchase_id:
            #         print("this is not CBD 2 already exists")
            #         purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
            #                                                               transaction_no=transaction_no,
            #                                                               transaction_date=paid_date,
            #                                                               purchase_id=purchase_id,
            #                                                               amount_receive_payment=paid_amount,
            #                                                               payment_method_name=payment_method)
            #
            #         # pprint.pprint(purchase_order_payment)
    else:
        # print('invoices data is not available')
        purchase_invoice_response['message']='invoices data not available'

    payments = data.get('payments', None)
    if payments:
        delivery_no = delivery['delivery_no']
        get_purchase_invoice_id = get_purchase_delivery(delivery_no).json().get('purchase_delivery', None)['purchase_invoice']['id']
        if get_purchase_invoice_id and str(term_name) != 'CBD':
            delivery_no = delivery['delivery_no']
            get_purchase_invoice_id = get_purchase_delivery(delivery_no).json()['purchase_delivery']['purchase_invoice']['id']
            checking_invoice = get_purchase_invoice(get_purchase_invoice_id).json()
            if checking_invoice.get("purchase_invoice", None) and str(checking_invoice['purchase_invoice']['transaction_status']['name']).strip().lower() != 'paid':
                # create payment
                print('this is not CBD 3')
                purchase_order_payment = add_receive_payment_purchase(person_name=person_name,
                                                                      transaction_no=transaction_no,
                                                                      transaction_date=paid_date,
                                                                      purchase_id=
                                                                      checking_invoice['purchase_invoice'][
                                                                          'transaction_no'],
                                                                      amount_receive_payment=paid_amount,
                                                                      payment_method_name=payment_method,
                                                                      memo=transaction_no,
                                                                      bank=bank)

                # # pprint.pprint(purchase_order_payment)
                if purchase_order_payment.get('error_full_messages', None):
                    purchase_payment_response['message'] = 'failed'
                    purchase_payment_response['jurnal_message'] = purchase_order_payment['error_full_messages']
                    print('payment failed')
                    # pprint.pprint(purchase_order_payment)
                    print('paid ammount: %s' % paid_amount)
                    return transaction_response
                else:
                    purchase_payment_response['purchase_payment_id'] = purchase_order_payment['purchase_payment']['custom_id']
                    purchase_payment_response['message'] = 'created'
            else:
                purchase_payment_response['purchase_payment_id'] = get_purchase_invoice_id
                purchase_payment_response['message'] = 'already exists'
        else:
            print('this is CBD, payments data not processed')
            purchase_payment_response['message'] = 'this is CBD, payments data not processed'
            if get_purchase_invoice_id:
                checking_invoice = get_purchase_invoice(get_purchase_invoice_id).json()
                status_invoice = checking_invoice['purchase_invoice']['transaction_status']['name']
                print('status invoice: %s' % status_invoice)
                remaining = get_purchase_invoice(get_purchase_invoice_id).json()['purchase_invoice']['remaining']
                if float(remaining) > 0.0 and str(status_invoice).lower().strip() == 'overdue':
                    purchase_order_payment_2 = add_receive_payment_purchase(person_name=person_name,
                                                                          transaction_no=transaction_no,
                                                                          transaction_date=due_date,
                                                                          purchase_id=
                                                                          checking_invoice['purchase_invoice'][
                                                                              'transaction_no'],
                                                                          amount_receive_payment=remaining,
                                                                          payment_method_name=payment_method,
                                                                          memo=transaction_no,
                                                                          bank=bank)
                    if purchase_order_payment_2.get('error_full_messages', None):
                        purchase_payment_response['message'] = 'purchase_order_payment_2 failed'
                        purchase_payment_response['jurnal_message'] = purchase_order_payment_2['error_full_messages']
                        return transaction_response
    else:
        # print('payments data not available')
        purchase_payment_response['message'] = 'payment data not available'
    return transaction_response




def purchase_insert_bulk(filename, index=0, until=0):

    with open('order_response.json', 'w') as output_file:
        json.dump([], output_file)

    json_data = open('order_response.json', 'r')
    json_data = json.load(json_data)

    def go_bulk():
        try:
            pur = purchase(data=data)
            pprint.pprint(pur)
            return pur
        except Exception as e:
            tb = traceback.format_exc()
            print(f'purchase failed at index {current}')
            print(f'error message: {e.__repr__()}')
            print(f'traceback: {tb}')
            with open('index.txt', 'w') as i:
                i.write(str(current))
                i.close()
            time.sleep(5)
            with open('index.txt', 'w') as i:
                i.write('')
                i.close()
            return None

    report = []

    with open('index.txt', 'r') as i:
        index_log = i.read()
        index_log = int(index_log) if len(index_log) is not 0 else 0
        i.close()

    current = index if index is not 0 else index_log

    if until == 0:
        sample_data = read_sample_input(filename)[current:]
    else:
        sample_data = read_sample_input(filename)[current:until]
    for data in sample_data:
        print('------------------transaction no: %s index: %s------------------' % (data['order_no'], current))
        run = go_bulk()
        while run == None:
            run = go_bulk()
        p = run
        current += 1
        if p:
            with open('order_response.json', 'w') as f:
                # writer = csv.writer(f)
                # writer.writerow()
                json_data.append(p)
                json.dump(json_data, f)

    print('insert purchase bulk finished.')
    with open('index.txt', 'w') as i:
        i.write('')
        i.close()
    return report